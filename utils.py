import os
import itertools

from PIL import Image
import numpy as np
import matplotlib.pyplot as plt


PROJECT_PATH = os.path.dirname(__file__)

CHANNELS = 'rgb'
PICTURE_SIZE = 100

INPUT_COLUMNS = ['%s%i' % (color, i)
                 for color in CHANNELS
                 for i in range(PICTURE_SIZE ** 2)]


def get_picture_data(picture_path):
    picture = Image.open(picture_path)
    picture = picture.resize((PICTURE_SIZE, PICTURE_SIZE), Image.ANTIALIAS)
    picture_data = np.array(list(zip(*picture.getdata()))).reshape(len(INPUT_COLUMNS))

    return picture_data


def project_path(*dirs):
    return os.path.join(PROJECT_PATH, *dirs)


def show_images(samples, title=None):
    for index, sample in samples.iterrows():
        if title is not None:
            plt.title(str(sample[title]))
        sample_as_grid = (sample[INPUT_COLUMNS].values.astype(np.int) / 255).reshape(len(CHANNELS), PICTURE_SIZE, PICTURE_SIZE)
        sample_as_grid = np.transpose(sample_as_grid, (1, 2, 0))
        plt.axis('off')
        plt.imshow(sample_as_grid, interpolation='nearest')
        plt.show()


def plot_confusion_matrix(cm, classes, title='Confusion matrix', cmap=plt.cm.Blues):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    # Normalized confusion matrix
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()

def extract_inputs(dataframe):
    standarized = dataframe[INPUT_COLUMNS].values.astype(np.int) / 255
    return standarized.reshape(len(dataframe), len(CHANNELS), PICTURE_SIZE, PICTURE_SIZE)
