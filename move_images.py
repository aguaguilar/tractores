import os
import shutil

import pandas as pd


def move_files(label, file_name):
	origen = os.path.join('flask', 'img', file_name)
	print(origen)
	if label == 0:
		shutil.copyfile(origen, "./Izquierda_adelante/"+file_name)
	elif label == 1:
		shutil.copyfile(origen, "./Derecha_adelante/"+file_name)
	else:
		shutil.copyfile(origen, "./Otros/"+file_name)
def main(path_csv):
	data = pd.read_csv(path_csv)
	data_files_name = list(data['picture_path'])
	data_labels = list(data['categoria'])

	for index, label in enumerate(data_labels):
		print("Processing {}/{}".format(index+1, len(data_labels)))
		move_files(label, data_files_name[index])

if __name__ == "__main__":
	main("flask/tractores.csv")
