import os

from flask import Flask, request, render_template, send_from_directory, url_for, jsonify, redirect
from werkzeug import secure_filename
import pandas as pd

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

app.config['ALLOWED_EXTENSIONS'] = ['jpg']

CSV_PATH = "tractores.csv"

def save_tags(raw_data, csv_path):
    print('Saving tagged rectangles to csv file...')
    columns = [
        'picture_path',
        'categoria',
    ]

    data = pd.DataFrame(raw_data, columns=columns)
    print(data)

    if os.path.exists(csv_path):
        print('Adding old data...')
        old_data = pd.read_csv(csv_path)
        data = pd.concat([old_data, data])

    data.to_csv(csv_path, index=False)
    print('Done')


def allowed_file(filename):
    return '.' in filename and filename.split('.')[1] in app.config['ALLOWED_EXTENSIONS']


def get_images(csv_path, path_images):
    images = []
    if os.path.exists(csv_path):
        data = pd.read_csv(csv_path)
        processed_images = list(data['picture_path'])
        for img in os.listdir(path_images):
            if img not in processed_images and allowed_file(img):
                images.append(img)
        return images
    else:
        return os.listdir(path_images)


#  begin statics

def dated_url_for(endpoint, **values):
    if endpoint == 'js_static':
        filename = values.get('filename', None)
        if filename:
            file_path = os.path.join(app.root_path,
                                     'static/js', filename)
            values['q'] = int(os.stat(file_path).st_mtime)
    elif endpoint == 'css_static':
        filename = values.get('filename', None)
        if filename:
            file_path = os.path.join(app.root_path,
                                     'static/css', filename)
            values['q'] = int(os.stat(file_path).st_mtime)
    return url_for(endpoint, **values)


@app.route('/css/<path:filename>')
def css_static(filename):
    '''
    :param filename ejemplo, css/styles.css:
    :return: ruta al archivo css estatico
    '''
    return send_from_directory(app.root_path + '/static/css/', filename)


@app.route('/js/<path:filename>')
def js_static(filename):
    return send_from_directory(app.root_path + '/static/js/', filename)


@app.route('/img/<path:filename>')
def jpg_static_logo(filename):
    '''
    :param filename:
    :return: rutas estaticas al jpg para mostrar como resultado.
    '''
    return send_from_directory(app.root_path + '/img/', filename)

#  end static

@app.route('/')
def index():
    '''
    :return: devuelvel html del inicio
    '''
    return render_template('index.html', 
        images=get_images(csv_path=CSV_PATH, path_images="img/")[:50])


@app.route('/uploadajax', methods=['POST'])
def uploadfile():
    '''
    :return: recibe un formdata con el nombre del archivo y los parametros
     hace el tratamiento de la imagen y la devuelve con un json.
    '''
    if request.method == 'POST':
        print("DATOS MIOS")
        #print(len(list(request.form)))
        #save_tags(list(request.data), CSV_PATH)
        raw_data = []
        for index, value in request.json.items():
            raw_data.append([index, value])
        save_tags(raw_data, CSV_PATH)

        return redirect('/')

if __name__ == '__main__':
    app.run(debug=True)
