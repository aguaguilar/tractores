import os

def rename(path):
	for index, img in enumerate(os.listdir(path)):
		os.rename(os.path.join(path, img), os.path.join(path, str(index) + ".jpg"))
		print("{}/{}".format(index, len(os.listdir(path))))

if __name__ == '__main__':
	rename('flask/img/')