"""
Generate pkl from images:

Usage:
    python generate_pkl images_set

Options:
    image_set -> derecha / izquierda / otros

Result:
    In the project root folder you will find the *.pkl files.
"""

import sys
from os import listdir, path

import pandas as pd

from utils import INPUT_COLUMNS, get_picture_data


def img_to_pkl(path_img, size=100, subset="otros"):
    if subset == 'izquierda':
        print('Procesando izquierda!')
        pkl_name = 'izquierda.pkl'
        label = 0
    elif subset == 'derecha':
        print('Procesando derecha!')
        pkl_name = 'derecha.pkl'
        label = 1
    else:
        print('Procesando otros!')
        pkl_name = 'otros.pkl'
        label = 2

    file_names = []
    for file_name in listdir(path_img):
        extension = file_name.split('.')[-1].lower()
        if extension in ('png', 'jpg', 'jpeg'):
            file_names.append(file_name)

    def pictures_data_generator():
        for index, file_name in enumerate(file_names):
            print('Processing image {0}/{1}'.format(str(index), len(file_names)))
            picture_path = path.join(path_img, file_name)
            yield get_picture_data(picture_path)

    pictures_data = pd.DataFrame(pictures_data_generator(), columns=INPUT_COLUMNS)
    pictures_data['label'] = label
    pictures_data['file_name'] = file_names

    print('Converting to pkl file')
    pictures_data.to_pickle(pkl_name)

if __name__ == '__main__':
    params = sys.argv
    if len(params) == 2:
        if params[1] not in ('todos', 'otros', 'izquierda', 'derecha'):
            print('El parametro es incorrecto')
            exit()
        if params[1] in ('todos', 'otros'):
            img_to_pkl('./Otros/', subset="otros")
        if params[1] in ('todos', 'izquierda'):
            img_to_pkl('./Izquierda_adelante/', subset='izquierda')
        if params[1] in ('todos', 'derecha'):
            img_to_pkl('./Derecha_adelante/', subset='derecha')
    else:
        print(__doc__)
